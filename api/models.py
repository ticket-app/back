from django.db import models

# Create your models here.

class Company(models.Model):
    name = models.CharField(max_length=100, null=True)
    nit = models.CharField(max_length=12, null=True)
    phone = models.CharField(max_length=12, null=True)
    address = models.CharField(max_length=60, null=True)
    mail = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=120, null=True)
    description = models.CharField(max_length=300, null=True)   
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)    

    def __str__(self):
        return self.name

class UserHistory(models.Model):
    name = models.CharField(max_length=120, null=True)
    description = models.CharField(max_length=300, null=True)   
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class Ticket(models.Model):
    STATE = (
        ('A','Active'),
        ('P','In process'),
        ('C','Completed'),
    )
    name = models.CharField(max_length=120, null=True)
    description = models.CharField(max_length=300, null=True)
    state = models.CharField(max_length=1, choices=STATE, null=True)
    user_history = models.ForeignKey(UserHistory, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name






    
