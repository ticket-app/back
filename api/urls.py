from django.urls import path
from . import views

urlpatterns = [
    path('',views.api,name="api"),
    path('company-list/',views.companyList,name="company-list"),
    path('company-list/<str:pk>/',views.companySelect,name="company-select"),
    path('project-create/',views.projectCreate,name="project-create"),
    path('project-list/<str:pk>/',views.projectList,name="project-list"),
    path('project-select/<str:pk>/',views.projectSelect,name="project-select"),
    path('user-history-create/',views.userHistoryCreate,name="user-history-create"),
    path('user-history-list/<str:pk>/',views.userHistoryList,name="user-history-list"),
    path('user-history-select/<str:pk>/',views.userHistorySelect,name="user-history-select"),
    path('ticket-create/',views.ticketCreate,name="ticket-create"),
    path('ticket-list/<str:pk>/',views.ticketList,name="ticket-list"),
    path('ticket-select/<str:pk>/',views.ticketSelect,name="ticket-select"),   
    path('ticket-update/<str:pk>/',views.ticketUpdate,name="ticket-update"),
    path('ticket-delete/<str:pk>/',views.ticketDelete,name="ticket-delete"),
]