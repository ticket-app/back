from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from .models import *
# Create your views here.

@api_view(['GET'])
def api(request):
    api_urls = {
        'List company':'/company-list/',
        'List select company':'/company-list/?/',
        'Create project':'/project-create/',
        'List project':'/project-list/?/',
        'Select project':'/project-select/?/',
        'Create user history':'/user-history-create/',
        'List user history':'/user-history-list/?/',
        'Select user history':'/user-history-select/?/',
        'Create tickets':'/ticket-create/',
        'List select tickets':'/ticket-list/?/',
        'Update ticket':'/ticket-update/?/',
        'Delete ticket':'/ticket-delete/?/',
        'Select ticket':'/ticket-select/?/',
    }
    return Response(api_urls)

@api_view(['GET'])
def companyList(request):
    company = Company.objects.all()
    serializer = CompanySerializer(company, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def companySelect(request,pk):
    company = Company.objects.get(id=pk)
    serializer = CompanySerializer(company, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def projectCreate(request):
    serializer = ProjectSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['GET'])
def projectList(request,pk):
    project = Project.objects.filter(company__name=pk)
    serializer = ProjectSerializer(project, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def projectSelect(request,pk):
    project = Project.objects.get(id=pk)
    serializer = ProjectSerializer(project, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def userHistoryCreate(request):
    serializer = UserHistorySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)
 
@api_view(['GET'])
def userHistoryList(request,pk):
    user_history = UserHistory.objects.filter(project__id=pk)
    serializer = UserHistorySerializer(user_history, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def userHistorySelect(request,pk):
    user_history = UserHistory.objects.get(id=pk)
    serializer = UserHistorySerializer(user_history, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def ticketCreate(request):
    serializer = TicketSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)
 
@api_view(['GET'])
def ticketList(request,pk):
    ticket = Ticket.objects.filter(user_history__id=pk)
    serializer = TicketSerializer(ticket, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def ticketSelect(request,pk):
    ticket = Ticket.objects.get(id=pk)
    serializer = TicketSerializer(ticket, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def ticketUpdate(request,pk):
    ticket = Ticket.objects.get(id=pk)
    serializer = TicketSerializer(instance=ticket, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def ticketDelete(request,pk):
    ticket = Ticket.objects.get(id=pk)
    if ticket.state == "A":
        ticket.delete()
    else :
        return Response("It's not possible delete the task because not be in active state")
    return Response("task deleted")